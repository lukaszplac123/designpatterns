/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observator.myObservator;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Lukasz
 */
public class Newsweek implements MyObservableObjects {

    //numer wydania
    private int number;
    private String newspaperNumbers;
    
    private List<ObserversObjects> observers;

    public List<ObserversObjects> getObservers() {
        return observers;
    }
    
    public Newsweek(){
        observers = new ArrayList<ObserversObjects>();
        number = 0;
        newspaperNumbers = "Newsweek";
    }
    
    @Override
    public void addObserver(ObserversObjects observer) {
           observers.add(observer);
    }

    @Override
    public void removeObserver(ObserversObjects observer) {
           int i = observers.indexOf(observer);
           observers.remove(i);
    }

    @Override
    public void informObservers() {
        for (ObserversObjects observer : observers){
            observer.updateState();
        }
    }

    @Override
    public void stateChanged() {
        StringBuilder string = new StringBuilder();
        this.number++;
        string.append(this.newspaperNumbers + " " + number);
        this.newspaperNumbers = string.toString();
        this.informObservers();
    }

    @Override
    public String toString() {
        return this.newspaperNumbers; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
