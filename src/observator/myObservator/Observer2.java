/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observator.myObservator;

/**
 *
 * @author Lukasz
 * Observer2 ma mozliwosc observowania czasopisma dostarzonego
 * w konstruktorze jako obiekt obserwowany
 * MyObservable objcets
 */
public class Observer2 implements ObserversObjects{

    private MyObservableObjects newspaper;

    public Observer2(MyObservableObjects newspaper) {
        this.newspaper = newspaper;
        this.newspaper.addObserver(this);
    }
            
    @Override
    public void updateState() {
        System.out.println(this.getName() + " possess " + this.newspaper.toString());
    }
    
    private String getName(){
        return "Observer2";
    }
    
}
