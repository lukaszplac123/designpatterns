/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observator.myObservator;

/**
 *
 * @author Lukasz
 */
public interface MyObservableObjects {
    
    public void addObserver(ObserversObjects observer);
    public void removeObserver(ObserversObjects observer);
    public void informObservers();
    public void stateChanged();

    @Override
    public String toString();
    
    
}
