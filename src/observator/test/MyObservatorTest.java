/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observator.test;

import observator.myObservator.Newsweek;
import observator.myObservator.Observer1;
import observator.myObservator.Observer2;
import observator.myObservator.Observer3;
import observator.myObservator.Times;

/**
 *
 * @author Lukasz
 */
public class MyObservatorTest {
    public static void main(String[] args) {
      
        //tworzenie czasopism
        Times times = new Times();
        Newsweek newsweek = new Newsweek();
        
        //towrzenie obserwatorow, 1,3 - obserwuja Timesa, 2-obserwuje Newsweek
        Observer1 observer1 = new Observer1(times);
        Observer2 observer2 = new Observer2(newsweek);
        Observer3 observer3 = new Observer3(times);
        
        //zmiany stanow obiektowo obserwowanych
        //obserwatorzy powinni zostac poinformowani i wyswietlic jakie posiadaja numey gazet
        times.stateChanged();
        times.stateChanged();
        newsweek.stateChanged();
        times.stateChanged();
        newsweek.stateChanged();
      
    }
}
