/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 * Wzorzec Singleton
 * W jednowątkowych aplikacjach jego implementacja wygladalaby jak ponizej.
 * Jezeli z singletona mialoby korzystac wiele watkow jego implementacje nalezaloby zmienic
 * wg metody 1 lub po prostu zsynchronizowac metode getInstance (niekiedy jednak takie cos jest malo wydajne
 * kiedy watki czesto odwoluja sie do instncji ze wzgledu na narzut jaki synchronizacja powoduje wtedy lepiej 
 * uzyc metody 1)
 */
public class Singleton {
    
    private static Singleton uniqeInstance;
    
    private Singleton(){}
    
    public static Singleton getInstance(){
        if (uniqeInstance == null){
            uniqeInstance = new Singleton();
        }
        return uniqeInstance;
    }
    
}

//Metoda 1 - tworzenie instancji zaraz po zaladowaniu klasy. Pozniej gdy watki
//chca sie do niej odwolac istnieje juz jedyna w swoim rodzaju instancja
//public class Singleton {
//    
//    private static Singleton uniqeInstance = new Singleton();
//    
//    private Singleton(){}
//    
//    public static Singleton getInstance(){
//        return uniqeInstance;
//    }    
//}
