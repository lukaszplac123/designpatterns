/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author Lukasz
 */
public class Test {
    
    public static void main(String[] args) {
          Singleton uniqe1 = Singleton.getInstance();
          System.out.println("unique1 = " + uniqe1);
          Singleton uniqe2 = Singleton.getInstance();
          System.out.println("unique2 = " + uniqe2);
          System.out.println("Porownaj nazwy... Obie referencje wskazuja na ten sam jeden obiekt");
    }
  
}
