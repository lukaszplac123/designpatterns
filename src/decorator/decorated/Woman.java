/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator.decorated;

/**
 *
 * @author Lukasz
 */
public class Woman extends Human {

    public Woman() {
        description = "I am a woman. ";
    }

    @Override
    public double getCost() {
        return 0; //koszt 0 gdyz sam "obiekt" kobieta jest poczatkowo nieubrany :)
    }
}
