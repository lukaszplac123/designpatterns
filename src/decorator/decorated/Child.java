/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator.decorated;

/**
 *
 * @author Lukasz
 */
public class Child extends Human {

    public Child() {
        description = "I am a child. ";
    }

    @Override
    public double getCost() {
        return 0;
    }
    
}
