/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator.decorated;

/**
 *
 * @author Lukasz
 */
public abstract class Human {
    
    protected String description = "I am a human.";
    
    public String getDescription(){
        return description;
    }
    
    public abstract double getCost(); 
    
}
