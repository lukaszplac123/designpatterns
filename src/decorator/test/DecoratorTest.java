/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator.test;

import decorator.decorated.Child;
import decorator.decorated.Human;
import decorator.decorated.Woman;
import decorator.decorators.Trousers;
import decorator.decorators.Underwear;



/*
Wzorzec dekorator.
Dekorujemy poczatkowe obiekty kobiete i dziecko poprzez dekoratory.
Jest to mozliwe gdyz obiekty dekorator posiadaja referencje do obiektow z ktorych dziedzica
(Woman, child dziedzicza po Human oraz Decorator tez dziedziczy po Human dzieki czemu zachowujemy
zgodnosc typow). 
*/

public class DecoratorTest {
    public static void main(String[] args) {
        
        Human women = new Trousers(new Underwear(new Woman(), "majtkiXXL", 1.44),"spodnieXXL", 10.55);
        System.out.println(women.getDescription() + " " + women.getCost());
        
        Human child = new Underwear(new Trousers(new Child(), "spodnieXXS", 5.44),"majtkiXXS", 0.55);
        System.out.println(child.getDescription() + " " + child.getCost());
        
    }
}
