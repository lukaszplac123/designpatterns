/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator.decorators;

import decorator.decorated.Human;

/**
 *
 * @author Lukasz
 */
public class Underwear extends Decorator{

    
    public Underwear(Human human, String description, double cost){
        this.human = human;
        this.description = description;
        this.cost = cost;
    }
    
    @Override
    public String getDescription() {
        return human.getDescription() + "I wear " + this.description+".";
    }

    @Override
    public double getCost() {
        return human.getCost() + this.cost;
    }
    
}
