/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator.decorators;

import decorator.decorated.Human;


/**
 *
 * @author Lukasz
 */
public abstract class Decorator extends Human {
    
    protected String description;
    protected Double cost;
    protected Human human;
    
    public abstract String getDescription();
}
