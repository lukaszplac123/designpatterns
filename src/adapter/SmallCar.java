/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Lukasz
 */
public class SmallCar implements Car{

    @Override
    public void move() {
        System.out.println("I move at speed 70km/h");
    }

    @Override
    public void refuel() {
        System.out.println("Refueling: + 40l");
    }
    
}
