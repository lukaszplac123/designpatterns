/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Lukasz
 */
public class Test {
    public static void main(String[] args) {
        Aircraft jet = new Jet();
        Car opelCorsa = new SmallCar();
        Tank tiger = new CombatTank();
        
        opelCorsa.move();
        opelCorsa.refuel();
        
        //zalozmy ze chcemy uzywac metod samolotu za pomoca metod samochodu
        //nalezy uzyc adaptera samolotu implementujacego interfejs samochodu
        AircraftAdapter aircraftAdapter = new AircraftAdapter(jet);
        aircraftAdapter.move();
        aircraftAdapter.refuel();
        
        //zalozmy ze chcemy uzywac metod czolgu za pomoca metod samochodu
        //nalezy uzyc adaptera czolgu implementujacego interfejs samochodu
        TankAdapter tankAdapter = new TankAdapter(tiger);
        tankAdapter.move();
        tankAdapter.refuel();
        tankAdapter.attack(); //dodatkowo umozliwiamy rowniez atakowanie za pomoca tego adaptera, poprzez wywolanie metody attack() czolgu
    }
}
