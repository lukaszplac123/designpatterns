/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Lukasz
 */
public class TankAdapter implements Car{
    Tank tank;

    public TankAdapter(Tank tank) {
        this.tank = tank;
    }
    
    
    @Override
    public void move() {
        tank.move();
    }

    @Override
    public void refuel() {
        tank.refuel();
    }
    
    public void attack(){
        tank.attack();
    }
}
