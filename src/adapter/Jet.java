/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Lukasz
 */
public class Jet implements Aircraft{

    @Override
    public void fly() {
        System.out.println("I am flying very fast");
    }

    @Override
    public void refuel() {
        System.out.println("Refuelenig: + 1000l");
    }
    
}
