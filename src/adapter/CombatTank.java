/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Lukasz
 */
public class CombatTank implements Tank {

    @Override
    public void move() {
        System.out.println("I am tank. I am moving slowly");
    }

    @Override
    public void refuel() {
        System.out.println("Refueling tank: Takes a lot of time + 2000l");
    }

    @Override
    public void attack() {
        System.out.println("I am attacking the enemy");
    }
    
}
