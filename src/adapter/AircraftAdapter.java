/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author Lukasz
 */
public class AircraftAdapter implements Car{

    Aircraft aircraft;

    public AircraftAdapter(Aircraft aircraft) {
        this.aircraft = aircraft;
    }
    
    @Override
    public void move() {
        aircraft.fly();
    }

    @Override
    public void refuel() {
        aircraft.refuel();
    }
    
}
