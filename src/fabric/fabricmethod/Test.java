/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabric.fabricmethod;

/**
 * Fabryka samochodow - wzorzec metoda fabrykujaca
 * W zalezonsci od tego jakiej fabryki uzyjemy metoda ordercar wywolywana w
 * klasie abstrakcyjnej CarFabric wywola metode fabrykującą makeCar ktora na podstawie otrzymanego
 * parametu stworzy odpowiedni samochod typu Car
 */
public class Test {
    public static void main(String[] args) {
        CarFabric frenchFabric = new FrenchFabric();
        CarFabric americanFabric = new AmericanFabric();
        Car car1 = frenchFabric.orderCar("Peugeot");
        Car car2 = frenchFabric.orderCar("Citroen");
        Car car3 = americanFabric.orderCar("Chrysler");
        Car car4 = americanFabric.orderCar("Ford");
        
        System.out.println(car1.getName());
        System.out.println(car2.getName());
        System.out.println(car3.getName());
        System.out.println(car4.getName());
        
        
    }
}
