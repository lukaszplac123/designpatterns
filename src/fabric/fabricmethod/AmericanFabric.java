/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabric.fabricmethod;

/**
 *
 * @author Lukasz
 */
public class AmericanFabric extends CarFabric{

    @Override
    public Car makeCar(String type) {
        if (type.equals("Ford")) return new Ford(type);
        else if (type.equals("Chrysler")) return new Chrysler(type);
        else return null;
    }
    
}
