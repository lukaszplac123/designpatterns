/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabric.fabricmethod;

/**
 *
 * @author Lukasz
 */
public class FrenchFabric extends CarFabric{

    @Override
    public Car makeCar(String type) {
        if (type.equals("Citroen")) return new Citroen(type);
        else if (type.equals("Peugeot")) return new Peugeot(type);
        else return null;
    }
    
}
