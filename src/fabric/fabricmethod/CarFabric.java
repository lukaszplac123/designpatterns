/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabric.fabricmethod;

/**
 *
 * @author Lukasz
 */
public abstract class CarFabric {

    public Car orderCar(String type){
        Car car = makeCar(type);
        return car;
    }
    
    
    //metoda fabrykujaca
    public abstract Car makeCar(String type);
}
