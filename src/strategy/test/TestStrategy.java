/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.test;

import strategy.behaviours.Hunt;
import strategy.behaviours.Sport;
import strategy.objects.Child;
import strategy.objects.Human;
import strategy.objects.Men;
import strategy.objects.Woman;

/**
 *
 * @author Lukasz
 * 
 * Program testuje wzorzez projektowy Strategy.
 * 
 * Wydzielone zostaly w osobnych pakietach informacje o obiektach Human od rodzajow czynnosci wykonywanych przez te obiekty.
 * Human dzieki przechowywaniu referencji do interfejsow moze zmieniac "w locie" czynnosci jakie moze wykonywac poprzez
 * metody usatwiajace te czynnosci. Do metod ustawiajacych przekazywane sa obiekty klas, ktore implementuja odpowiednie interfejsy
 * czynnosci. W przyszlosci moga byc dodawane kolejne czynnosci jakie moga byc wykonywane przez poszczegolnych ludzi.
 * Bez wzgledu jednak na to kazdy czlowiek(Human) posiada mozliwosc usmiechu. Metoda smile() jest dziedziczona przez kazdy obiekt
 * i nie jest to zachowanie specyficzne - nie warto zatem tworzyc odpowiedniego interfejsu obslugujacego ta czynnosc.
 * 
 * 
 */
public class TestStrategy {
    
    public static void main(String[] args) {
        Human human = new Human("human");
        Human child = new Child("child");
        Human woman = new Woman("woman");
        Human men = new Men("men");
    
        
        System.out.println(men.getName());
        men.drive();
        men.sport();
        men.hunt();
        men.smile();
        
        System.out.println("--------------");
        
        men.setHuntingAbility(new Hunt());
        System.out.println(men.getName());
        men.drive();
        men.sport();
        men.hunt();
        men.smile();
        
        System.out.println("--------------");
        
        System.out.println(child.getName());
        child.drive();
        child.sport();
        child.hunt();
        child.smile();
        
        System.out.println("-----child----");
        
        child.setSportAbility(new Sport());
        System.out.println(child.getName());
        child.drive();
        child.sport();
        child.hunt();
        child.smile();
    }
    
}
