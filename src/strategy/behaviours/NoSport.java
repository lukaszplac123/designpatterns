/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.behaviours;

/**
 *
 * @author Lukasz
 */
public class NoSport implements SportBehaviour{

    @Override
    public void sport() {
        System.out.println("I can not do sports!");
    }
    
}
