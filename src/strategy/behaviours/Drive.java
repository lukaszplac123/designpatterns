/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.behaviours;

/**
 *
 * @author Lukasz
 */
public class Drive implements DrivingBehaviour{

    @Override
    public void drive() {
        System.out.println("I can drive!");
    }
     
}
