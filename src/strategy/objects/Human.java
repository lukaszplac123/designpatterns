/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.objects;

import strategy.behaviours.DrivingBehaviour;
import strategy.behaviours.HuntingBehaviour;
import strategy.behaviours.NoDrive;
import strategy.behaviours.NoHunt;
import strategy.behaviours.NoSport;
import strategy.behaviours.SportBehaviour;

public class Human {
    
    protected DrivingBehaviour drivingBehaviour;
    protected HuntingBehaviour huntingBehaviour;
    protected SportBehaviour sportBehaviour;
    
    protected String name;

    public Human(String name) {
        this.name = name;
        drivingBehaviour = new NoDrive();
        huntingBehaviour = new NoHunt();
        sportBehaviour = new NoSport();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setSportAbility(SportBehaviour sport){
        this.sportBehaviour = sport;
    }
    
    public void setHuntingAbility(HuntingBehaviour hunt){
        this.huntingBehaviour = hunt;
    }
    
    public void setDrivingAbility(DrivingBehaviour drive){
        this.drivingBehaviour = drive;
    }
    
    public void drive(){
        this.drivingBehaviour.drive();
    }

    public void sport(){
        this.sportBehaviour.sport();
    }

    public void hunt(){
        this.huntingBehaviour.hunt();
    }
    
    public void smile(){
        System.out.println(this.getName() + " can smile");
    }
    
}
