/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.devices;

/**
 *
 * @author Lukasz
 */
public class Fan {
    String status;

    public Fan() {
        status = "Fan turnedOff";
    }

    public void turnOn(){
        status = "Fan turnedOn";
        showStatus();
    }
    
    public void turnOff(){
        status = "Fan turnedOff";
        showStatus();
    }
    
    private void showStatus(){
        System.out.println(status);
    }
}
