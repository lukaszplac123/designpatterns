/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.devices;

/**
 *
 * urzadzenia wykonujace polecenia
 * Swiatlo
 */
public class Light {
    
    String status;

    public Light() {
        status = "Lights turnedOff";
    }

    public void turnOn(){
        status = "Lights turnedOn";
        showStatus();
    }
    
    public void turnOff(){
        status = "Lights turnedOff";
        showStatus();
    }
    
    private void showStatus(){
        System.out.println(status);
    }
}
