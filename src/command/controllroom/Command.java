/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.controllroom;

/**
 *
 * Ten interfejs bedzie uzywany przez obikety realizujace szczegolowo
 * odpowiednie polecenia
 */
public interface Command {
    public void apply();
}
