/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.controllroom;

import command.commands.EmptyCommand;

/**
 *
 * Controll room to klasa przechowujaca przyciski do ktorych
 * przypisywane jest w locie odpowiednie polecenie poprzez metode
 * setCommnad
 */
public class ControllRoom {
    Command[] buttons;

    public ControllRoom() {
        buttons = new Command[4];
        Command emptyCommand = new EmptyCommand();
        for (int i = 0; i < buttons.length; i++){
            buttons[i] = emptyCommand;
        }
    }
    
    public void setCommand(int buttonNumber, Command command){
        buttons[buttonNumber] = command;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < buttons.length; i++) {
            builder.append("button[" + i + "] : " + buttons[i].getClass().getName() + "\n");
        }
        return builder.toString();
    }
    
    
}
