/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.controllroom;

import command.commands.TurnFanOffCommand;
import command.commands.TurnFanOnCommand;
import command.commands.TurnLightsOffCommand;
import command.commands.TurnLightsOnCommand;

/**
 *
 * Klasa testujaca wzorzec.
 * Najpierw tworzona jest instancja Controllroom.
 * Pod przycisk przypiswyane jest odpowiednie polecenie
 * Nastepnie to polecenie jest wykonywane
 */
public class Test {
    
    public static void main(String[] args) {
        
        ControllRoom room = new ControllRoom();
        
        System.out.println(room.toString());
        room.setCommand(0, new TurnLightsOnCommand());
        room.setCommand(1, new TurnLightsOffCommand());
        room.setCommand(2, new TurnFanOnCommand());
        room.setCommand(3, new TurnFanOffCommand());
        System.out.println(room.toString());
        
        //zalacz swiatlo
        room.buttons[0].apply();
        
        //zalacz wentylator
        room.buttons[2].apply();
        
        //wylacz swiatlo
        room.buttons[1].apply();
        
        //wylacz wentylator
        room.buttons[3].apply();
    }
}
