/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.commands;

import command.controllroom.Command;
import command.devices.Fan;

/**
 *
 * polecenie wlaczenia swiatel
 */
public class TurnFanOffCommand implements Command{
    Fan fan;

    public TurnFanOffCommand() {
        fan = new Fan();
    }

    @Override
    public void apply() {
        fan.turnOff();
    }
 
    
}
